import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import controller.Implementations.LoginControllerImpl;
import model.cell.CellImpl;
import model.cell.CellPosition;
import model.person.guard.GuardImpl;
import view.Interfaces.LoginView;

/**
 * The main of the application.
 */
public final class Main {
	
	/**
     * Program main, this is the "root" of the application.
     * @param args
     * unused,ignore
     */
	 public static void main(final String... args){
		 //creo cartella in cui mettere i dati da salvare
		 String Dir = "res";
		 new File(Dir).mkdir();
		 //creo file per le guardie
		 File fg = new File("res/GuardieUserPass.txt");
		 //se il file non è stato inizializzato lo faccio ora
		 if(fg.length()==0){
			 try {
				initializeGuards(fg);
			} catch (IOException e) {
				e.printStackTrace();
			}
		 }
		 //leggo il file contenente le celle
		 File f = new File("res/Celle.txt");
		 //se il file non è ancora stato inizializzato lo faccio ora
		 if(f.length()==0){
			 try {
				initializeCells(f);
			} catch (IOException e) {
				e.printStackTrace();
			}
		 }
		 
		 //chiamo controller e view del login
		 new LoginControllerImpl(new LoginView());
	 }
	 
	 /**
	  * metodo che inizializza le celle
	  * @param f file in cui creare le celle
	  * @throws IOException
	  */
	 static void initializeCells(File f) throws IOException{

		 List<CellImpl>list=new ArrayList<>();
		 CellImpl c;
		 for(int i=0;i<50;i++){
			 if(i<20){
				  c = new CellImpl.Builder()
				 			.setId(i)
						  	.setPosition(CellPosition.FIRST_FLOOR)
						  	.setCapacity(4)
				  			.build();
			 }
			 else
				 if(i<40){
					  c = new CellImpl.Builder()
					 		.setId(i)
							.setPosition(CellPosition.SECOND_FLOOR)
							.setCapacity(3)
					  		.build();
				 }
				 else
					 if(i<45){
					  c = new CellImpl.Builder()
						 	.setId(i)
							.setPosition(CellPosition.THIRD_FLOOR)
							.setCapacity(4)
					  		.build();
				 }
					 else{
						  c = new CellImpl.Builder()
						 	.setId(i)
						 	.setPosition(CellPosition.UNDERGROUND)
							.setCapacity(1)
						  	.build();
					 }
			 list.add(c);
		 }
			FileOutputStream fo = new FileOutputStream(f);
			ObjectOutputStream os = new ObjectOutputStream(fo);
			os.flush();
			fo.flush();
			for(CellImpl c1 : list){
				os.writeObject(c1);
			}
			os.close();
	 }
	 
	 /**
	  * metodo che inizializza le guardie
	  * @param fg file in cui creare le guardie
	  * @throws IOException
	  */
	 static void initializeGuards(File fg) throws IOException{

		 String pattern = "MM/dd/yyyy";
		 SimpleDateFormat format = new SimpleDateFormat(pattern);
		 LocalDate date = null;
		try {
			date = LocalDate.of(1980, 1, 1);
		} catch (Exception e) {
			e.printStackTrace();
		}
		List<GuardImpl>list=new ArrayList<>();

		GuardImpl g1 = new GuardImpl.Builder()
		 				.setName("Oronzo")
						.setSurname("Cantani")
		 				.setBirthday(date)
						.setRank(1)
						.setTelephoneNumber("0764568")
						.setId(1)
						.setPassword("ciao01")
						.build();
		list.add(g1);
		GuardImpl g2=new GuardImpl.Builder()
		 				.setName("Emile")
						.setSurname("Heskey")
		 				.setBirthday(date)
						.setRank(2)
						.setTelephoneNumber("456789")
						.setId(2)
						.setPassword("asdasd")
						.build();
		list.add(g2);
		GuardImpl g3=new GuardImpl.Builder()
		 				.setName("Gennaro")
						.setSurname("Alfieri")
		 				.setBirthday(date)
						.setRank(3)
						.setTelephoneNumber("0764568")
						.setId(3)
		 				.setPassword("qwerty")
						.build();
		list.add(g3);
		FileOutputStream fo = new FileOutputStream(fg);
		ObjectOutputStream os = new ObjectOutputStream(fo);
		os.flush();
		fo.flush();
		for(GuardImpl g : list){
			os.writeObject(g);
		}
		os.close();
	 }
	 
}
