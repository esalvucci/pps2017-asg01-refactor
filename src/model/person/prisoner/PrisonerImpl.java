package model.person.prisoner;

import java.time.LocalDate;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

import model.cell.Cell;
import model.person.AbstractPerson;

/**
 * implementazione di un prigioniero
 */
public class PrisonerImpl extends AbstractPerson implements Prisoner{

	private final Collection<String> crimes;
	private final LocalDate imprisonmentStartDate;
	private final LocalDate imprisonmentEndDate;
	private final Cell cell;
	private final int id;

	/**
	 * Instantiates a new prisoner.
	 *
	 * @param name the name
	 * @param surname the surname
	 * @param id the id
	 * @param imprisonmentStartDate imprisonmentStartDate della reclusione
	 * @param imprisonmentEndDate imprisonmentEndDate della reclusione
	 */
	private PrisonerImpl(final String name, final String surname, final LocalDate birthday, final int id, final LocalDate imprisonmentStartDate,
						final LocalDate imprisonmentEndDate, final List<String> crimes, final Cell cell) {
		super(name, surname, birthday);
		this.id = id;
		this.imprisonmentStartDate = imprisonmentStartDate;
		this.imprisonmentEndDate = imprisonmentEndDate;
		this.crimes = crimes;
		this.cell = cell;
	}

	@Override
	public final void addCrime(final String crime) {
		crimes.add(crime);
	}

	@Override
	public final Collection<String> getCrimes(){
		return this.crimes;
	}

	@Override
	public final int getId() {
		return this.id;
	}

	@Override
	public final LocalDate getImprisonmentStartDate() {
		return this.imprisonmentStartDate;
	}

	@Override
	public final LocalDate getImprisonmentEndDate() {
		return imprisonmentEndDate;
	}

	@Override
	public final Cell getCell() {
		return cell;
	}

	@Override
	public final boolean equals(Object o) {
		if (this == o) {
			return true;
		}

		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		PrisonerImpl prisoner = (PrisonerImpl) o;
		return this.getId() == prisoner.getId() &&
				Objects.equals(this.getCrimes(), prisoner.getCrimes()) &&
				Objects.equals(this.getImprisonmentStartDate(), prisoner.getImprisonmentStartDate()) &&
				Objects.equals(this.getImprisonmentEndDate(), prisoner.getImprisonmentEndDate()) &&
				Objects.equals(this.getCell(), prisoner.getCell());
	}

	@Override
	public final int hashCode() {
		return Objects.hash(this.getCrimes(), this.getImprisonmentStartDate(),
				this.getImprisonmentEndDate(), this.getCell(), this.getId());
	}

	@Override
	public final String toString() {
		return "PrisonerImpl [id=" + id + ", imprisonmentStartDate=" + imprisonmentStartDate + ", imprisonmentEndDate=" + imprisonmentEndDate + ", crimes="
				+ crimes + "]";
	}

	public static class Builder {
		private String name;
		private String surname;
		private LocalDate birthday;
		private int id;
		private LocalDate imprisonmentStartDate;
		private LocalDate imprisonmentEndDate;
		private List<String> crimes;
		private Cell cell;

		public Builder setName(String name) {
			this.name = name;
			return this;
		}

		public Builder setSurname(String surname) {
			this.surname = surname;
			return this;
		}

		public Builder setBirthday(LocalDate birthday) {
			this.birthday = birthday;
			return this;
		}

		public Builder setId(int id) {
			this.id = id;
			return this;
		}

		public Builder setImprisonmentStartDate(LocalDate imprisonmentStartDate) {
			this.imprisonmentStartDate = imprisonmentStartDate;
			return this;
		}

		public Builder setImprisonmentEndDate(LocalDate imprisonmentEndDate) {
			this.imprisonmentEndDate = imprisonmentEndDate;
			return this;
		}

		public Builder setCrimes(List<String> crimes) {
			this.crimes = crimes;
			return this;
		}

		public Builder setCell(Cell cell) {
			this.cell = cell;
			return this;
		}

		public Prisoner build() {
			return new PrisonerImpl(this.name, this.surname, this.birthday, this.id, this.imprisonmentStartDate,
					this.imprisonmentEndDate, this.crimes, this.cell);
		}
	}
}
