package model.person.prisoner;

import model.cell.Cell;
import model.person.Person;

import java.time.LocalDate;
import java.util.Collection;

/**
 * interfaccia che rappresenta un prigioniero
 */
public interface Prisoner extends Person {

	/**
	 * Metodo che aggiunge un crimine alla lista dei crimini
	 * @return
	*/
	void addCrime(final String crime);

	/**
	 * Metodo che ritrna la lista dei crimini commessi da un criminale
	 * @return la lista dei crimini
	*/
	Collection<String> getCrimes();
	
	/**
	 * Metodo che ritorna l'id del prigioniero
	 * @return id del prigioniero
	*/
	int getId();

	/**
	 * Metodo che ritorna la data di inizio della reclusione
	 * @return data di inizio della reclusione
	*/
	LocalDate getImprisonmentStartDate();

	/**
	 * Metodo che ritorna la data di fine della reclusione
	 * @return data di fine della reclusione
	*/
	LocalDate getImprisonmentEndDate();

	/**
	 * Metodo che ritorna l'id della cella 
	 * @return id della cella
	 */
	Cell getCell();

}