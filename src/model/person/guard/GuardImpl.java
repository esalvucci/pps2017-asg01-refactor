package model.person.guard;

import java.time.LocalDate;
import java.util.Objects;
import model.person.AbstractPerson;

/**
 * implementazione di una guardia
 */
public final class GuardImpl extends AbstractPerson implements Guard {

	private static final long serialVersionUID = 3975704240795357459L;

	private final String telephoneNumber;
	private final String password;
	private final int rank;
	private final int id;

	/**
	 * costruttore di una guardia
	 * @param name nome
	 * @param surname cognome
	 * @param birthday data di nascita
	 * @param rank grado
	 * @param telephoneNumber numero di telefono
	 * @param id id guardia
	 * @param password password
	 */
	private GuardImpl(final String name, final String surname, final LocalDate birthday,
					  final int rank, final String telephoneNumber, final int id,
					  final String password) {
		super(name, surname, birthday);
		this.rank = rank;
		this.telephoneNumber = telephoneNumber;
		this.id = id;
		this.password = password;
	}

	@Override
	public final String getPassword() {
		return this.password;
	}

	@Override
	public final int getId() {
		return this.id;
	}

	@Override
	public final String getTelephoneNumber() {
		return telephoneNumber;
	}

	@Override
	public final int getRank() {
		return rank;
	}

	@Override
	public final boolean equals(Object o) {
		if (this == o) {
			return true;
		}

		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		if (!super.equals(o)) {
			return false;
		}

		GuardImpl guard = (GuardImpl) o;
		return this.getRank() == guard.getRank() &&
				this.getId() == guard.getId() &&
				Objects.equals(this.getTelephoneNumber(), guard.getTelephoneNumber()) &&
				Objects.equals(this.getPassword(), guard.getPassword());
	}

	@Override
	public final int hashCode() {
		return Objects.hash(super.hashCode(), this.getRank(), this.getTelephoneNumber(),
				this.getId(), this.getPassword());
	}

	@Override
	public final String toString() {
		return "GuardImpl [rank=" + rank + ", telephoneNumber=" + telephoneNumber
				+ ", id=" + id + "]";
	}

	public final static class Builder {

		private String telephoneNumber;
		private LocalDate birthday;
		private String password;
		private String surname;
		private String name;
		private int rank;
		private int id;

		public Builder() {

		}

		public Builder setTelephoneNumber(final String telephoneNumber) {
			this.telephoneNumber = telephoneNumber;
			return this;
		}

		public Builder setRank(final int rank) {
			this.rank = rank;
			return this;
		}

		public Builder setId(final int id) {
			this.id = id;
			return this;
		}

		public Builder setPassword(final String password) {
			this.password = password;
			return this;
		}

		public Builder setName(final String name) {
			this.name = name;
			return this;
		}

		public Builder setSurname(final String surname) {
			this.surname = surname;
			return this;
		}

		public Builder setBirthday(final LocalDate birthday) {
			this.birthday = birthday;
			return this;
		}

		public GuardImpl build() {
			if (this.telephoneNumber != null && this.password != null
					&& this.rank != 0 && this.id != 0) {
				return new GuardImpl(this.name, this.surname,
						this.birthday, this.rank, this.telephoneNumber,
						this.id, this.password);
			} else {
				throw new IllegalStateException();
			}

		}
	}
}
