package model.person.visitor;

import model.person.AbstractPerson;
import java.time.LocalDate;
import java.util.Objects;

/**
 * Implementazione di un visitatore
 */
public final class VisitorImpl extends AbstractPerson implements Visitor {

	/**
	 * id del prigioniero
	 */
	private final int prisonerId;
	
	/**
	 * 
	 * @param name nome
	 * @param surname cognome
	 * @param prisonerId id del prigioniero andato a trovare
	 */
	private VisitorImpl(final String name, final String surname, final LocalDate birthday, final int prisonerId) {
		super(name, surname, birthday);
		this.prisonerId = prisonerId;
	}

	@Override
	public final int getPrisonerId() {
		return this.prisonerId;
	}

    @Override
    public final boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        if (!super.equals(o)) {
            return false;
        }

        VisitorImpl visitor = (VisitorImpl) o;
        return Objects.equals(this.getPrisonerId(), visitor.getPrisonerId());
    }

    @Override
    public final int hashCode() {
        return Objects.hash(super.hashCode(), this.getPrisonerId());
    }

    @Override
	public final String toString() {
		return "VisitorImpl getName()=" + getName() + ", getSurname()="+ getSurname() + ", getBirthday()=" + getBirthday() + "[getIdPrisoner()=" + getPrisonerId() + "]";
	}

	public static final class Builder {

		private String name;
		private String surname;
		private LocalDate birthday;
		private int prisonerId;

		public Builder() {

		}

		public final Builder setName(final String name) {
			this.name = name;
			return this;
		}

		public final Builder setSurname(final String surname) {
			this.surname = surname;
			return this;
		}

		public final Builder setBirthday(final LocalDate birthday) {
			this.birthday = birthday;
			return this;
		}

		public final Builder setPrisonerId(final int prisonerId) {
			this.prisonerId = prisonerId;
			return this;
		}

		public final VisitorImpl build() {
			if (this.name != null && this.surname != null && this.birthday != null && this.prisonerId != 0) {
				return new VisitorImpl(this.name, this.surname, this.birthday, this.prisonerId);
			} else {
				throw new IllegalStateException();
			}
		}

	}

}
