package model.person;

import java.time.LocalDate;
import java.util.Date;
/**
 * questa interfaccia rappresenta una persona
 */
public interface Person {

	/**
	 * Metodo che ritorna il nome della persona.
	 * 
	 * @return the name
	 */
	String getName();

	/**
	 * Metodo che ritorna il cognome della persona.
	 * 
	 * @return the surname
	 */

	String getSurname();

	/**
	 * Metodo che ritorna la data di nascita dellla persona.
	 * 
	 * @return the birth date
	 */

	LocalDate getBirthday();
	/**
	 * Metodo che setta il nome della persona.
	 * 
	 * @return
	 */
	void setName(final String name);

	/**
	 * Metodo che setta il cognome della persona.
	 * 
	 * @return
	 */
	void setSurname(final String surname);

	/**
	 * Metodo che setta la data di nascita dellla persona.
	 * 
	 * @return
	 */
	void setBirthday(final LocalDate birthday);
}
