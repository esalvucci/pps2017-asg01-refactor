package model.person;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Date;
import java.util.Objects;

/**
 * implementazione di una persona
 */
public abstract class AbstractPerson implements Serializable, Person {

	private static final long serialVersionUID = -6542753884631178660L;

	private String name;
	private String surname;
	private LocalDate birthday;

	/**
	 * Instantiates a new person.
	 *
	 * @param name the name
	 * @param surname the surname
	 */
	public AbstractPerson(final String name, final String surname, final LocalDate birthday) {
		super();
		this.name = name;
		this.surname = surname;
		this.birthday = birthday;
	}

	public final void setName(final String name) {
		this.name = name;
	}

	public final void setSurname(final String surname) {
		this.surname = surname;
	}

	public final void setBirthday(final LocalDate birthday) {
		this.birthday = birthday;
	}

	public final String getName() {
		return name;
	}

	public final String getSurname() {
		return surname;
	}

	public final LocalDate getBirthday() {
		return birthday;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}

		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		AbstractPerson person = (AbstractPerson) o;
		return Objects.equals(this.getName(), person.getName()) &&
				Objects.equals(this.getSurname(), person.getSurname()) &&
				Objects.equals(this.getBirthday(), person.getBirthday());
	}

	@Override
	public int hashCode() {
		return Objects.hash(this.getName(), this.getSurname(), this.getBirthday());
	}

	@Override
	public String toString() {
		return "AbstractPerson [name=" + this.getName() + ", surname=" + this.getSurname() + ", birthDate=" + this.getBirthday() + "]";
	}
	
	
}
