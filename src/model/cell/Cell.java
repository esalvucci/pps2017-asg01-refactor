package model.cell;

import model.person.prisoner.Prisoner;

/**
 * interfaccia di una cella
 * @author Utente
 *
 */
public interface Cell {

	/**
	 * metodo che ritorna l'id i una cella
	 * @return
	 */
	int getId();
	
	/**
	 * metodo che ritorna la posizione di una cella
	 * @return posizione cella
	 */
	CellPosition getPosition();

	/**
	 * metodo che ritorna la capacità di una cella
	 * @return capacità massima di una cella
	 */
	int getCapacity();

	/**
	 * metodo che ritorna i prigionieri correnti di una cella
	 * @return prigionieri di una cella
	 */
	int getCurrentPrisonersNumber();

	void add(final Prisoner prisoner);

    void remove(final Prisoner prisoner);
}
