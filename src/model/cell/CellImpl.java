package model.cell;

import model.person.prisoner.Prisoner;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import java.util.Objects;

/**
 * implementazione di una cella
 */
public final class CellImpl implements Cell, Serializable {

    private static final long serialVersionUID = 9167940013424894676L;
    private final int id;
	private final int capacity;
	private final CellPosition position;
	private final Collection<Prisoner> prisoners;

	/**
	 * costruttore cella
	 * @param id id cella
	 * @param position posizione
	 * @param capacity capacità
	 */
	private CellImpl(final int id, final CellPosition position, final int capacity){
		this.id = id;
		this.position = position;
		this.capacity = capacity;
		this.prisoners = new HashSet<>();
	}

	@Override
	public final int getId() {
		return id;
	}

	@Override
	public final CellPosition getPosition() {
		return position;
	}

	@Override
	public final int getCapacity() {
		return capacity;
	}

	@Override
	public final int getCurrentPrisonersNumber() {
		return prisoners.size();
	}

	@Override
	public final void add(Prisoner prisoner) {
	    if (this.prisoners.size() < this.capacity) {
            prisoners.add(prisoner);
        } else {
	        throw new IndexOutOfBoundsException();
        }
    }

    @Override
    public final void remove(final Prisoner prisoner) {
        this.prisoners.remove(prisoner);
    }

    @Override
    public final boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CellImpl cell = (CellImpl) o;
        return this.getId() == cell.getId() &&
                this.getCapacity() == cell.getCapacity() &&
                this.getPosition() == cell.getPosition() &&
                Objects.equals(prisoners, cell.prisoners);
    }

    @Override
    public final int hashCode() {
        return Objects.hash(this.getId(), this.getCapacity(), this.getPosition(), prisoners);
    }

    @Override
	public final String toString() {
		return "CellImpl [id=" + id + ", position=" + position.toString() + ", capacity=" + capacity + ", currentPrisonersNumber="
				 + "]";
	}
	
	public final static class Builder {

	    private CellPosition position;
	    private int capacity;
	    private int id;

	    public Builder() {

        }

        public Builder setPosition(final CellPosition position) {
            this.position = position;
            return this;
        }

        public Builder setCapacity(final int capacity) {
            this.capacity = capacity;
            return this;
        }

        public Builder setId(final int id) {
            this.id = id;
            return this;
        }

        public CellImpl build() {
	        if (this.position != null && this.capacity != 0 && this.id != 0) {
	            return new CellImpl(this.id, this.position, this.capacity);
            } else {
	            throw new IllegalStateException();
            }
        }
    }
}
