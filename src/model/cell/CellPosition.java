package model.cell;

public enum CellPosition {

    FIRST_FLOOR("Primo piano"),
    SECOND_FLOOR("Secondo piano"),
    THIRD_FLOOR("Terzo piano"),
    UNDERGROUND("Piano sotterraneo, celle di isolamento");

    private final String name;

    CellPosition(final String name) {
        this.name = name;
    }

    @Override
    public final String toString() {
        return this.name;
    }
}
