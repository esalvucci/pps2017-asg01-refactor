package model.bankAccount;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * implementazione di un movimento
 */
public final class MovementImpl implements Serializable, Movement {

	public enum OperationType {
		ACCREDITATION,
		CHARGE;
	}

	private final LocalDate date = LocalDate.now();
	private final OperationType operationType;
	private final double amount;
	private final String causal;

	/**
	 * costruttore di movimento
	 * @param causal descrizione
	 * @param amount ammontare
	 *
	 */
	private MovementImpl(final String causal, final double amount, final OperationType operationType){
		this.amount = amount;
		this.causal = causal;
		this.operationType = operationType;
	}
	@Override
	public final String getCausal() {
		return this.causal;
	}

	@Override
	public final double getAmount() {
		return this.amount;
	}

	@Override
	public final OperationType getOperationType() {
		return this.operationType;
	}

	@Override
	public final LocalDate getDate(){
		return this.date;
	}

	@Override
	public final String toString() {
		return "MovementImpl [causal=" + causal + " operationType= "+ this.operationType.name() + ", amount=" + amount + ", date=" + date + "]";
	}

	public final static class Builder {
		private OperationType operationType;
		private double amount;
		private String causal;

		public Builder setCausal(final String causal) {
			this.causal = causal;
			return this;
		}

		public Builder setAmount(final double amount) {
			this.amount = amount;
			return this;
		}

		public Builder setOperationType(final OperationType operationType) {
			this.operationType = operationType;
			return this;
		}

		public MovementImpl build() {
			if (this.operationType != null || this.amount != 0 || this.causal != null) {
				return new MovementImpl(this.causal, this.amount, this.operationType);
			} else {
				throw new IllegalStateException();
			}
		}
	}
}
