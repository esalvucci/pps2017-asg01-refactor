package model.bankAccount;

import java.time.LocalDate;

public interface Movement {

	/**
	*ritorna la descrizione del movimento
	*@return description of the movement
	*/
	String getCausal();

	/**
	*ritorna la quantita del movimento
	*@return amount of the movement
	*/
	double getAmount();

	/**
	 * ritorna se il movimento � positivo o negativo
	 * @return the sign of the movement (+ or -)
	 */
	MovementImpl.OperationType getOperationType();
	
	/**
	 * ritorna la data del movimento
	 * @return the date of the movement
	 */
	LocalDate getDate();

}
